from django.urls import path
from . import views

app_name = 'generator'

urlpatterns = [
    path('generate/', views.Generator.as_view(), name='generate'),
]
