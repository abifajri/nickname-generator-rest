from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework import status
import random

# Create your views here.
class Generator(APIView):
    def get(self, request, format=None):

        nickname_list = ["Claws", "Hela", "Selletta", "BeeValued", "Pregustation", "Ulem", "Padir", "Maldrie",
            "PoolTeenie", "Acolyte", "Sunri", "Bilirubin", "ByteParis", "Fusertis", "Westrach", "Siliem", "Nankeen",
            "Nankeen", "Desmina", "Onra", "Phiave", "Wonrulam", "Misri", "Vinlia", "Phealvar", "Lisire"]

        answer = random.choice(nickname_list)
        try:
            return Response(answer)
            
        except:
            return Response({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)